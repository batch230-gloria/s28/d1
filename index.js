// CRUD Operations (Create-Read-Update-Delete)
/*
	- CRUD operations are the heart of any back-end application
	- Mastering the CRUD operations is essential for any developer
	- This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
	- Mastering the CRUD operations of any language makes us a valueable developer and makes the work easier for us to deal with huge amounts of information
*/



// [SECTION] Inserting Documents (CREATE)
/*
	Syntax:
		- db.collectionName.insertOne({object});
*/

// Insert One (Inserts one document)
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact :{
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Inser Many (Inserts one or more documents)
/*
	Syntax:
		- db.collectionName.insertMany([ {obejctA}, {objectB} ]);
*/

db.users.insertMany(
	[
		{	
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact :{
					phone: "87000",
					email: "stephenhawking@mail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{	
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact :{
					phone: "4770220",
					email: "neilarmstrong@mail.com"
			},
			courses: ["React", "Laravel", "MongoDB"],
			department: "none"
		},
	]
)




// -----------------------------------------------------------------
// [SECTION] Finding Documents (READ)
/*
	Syntax:
		- db.collectionName.find();
		- db.collectionName.find({field: value});
*/


// retrieve all documents
db.users.find();

// retrieve specific document/s
db.users.find({firstName: "Stephen"});

// multiple criteria
db.users.find({lastName: "Armstrong", age:70});







// -----------------------------------------------------------------
// [SECTION] Update and Replace (UPDATE)
/*
	Syntax:
		- db.collectionName.updateOne({criteria}, {$set: {field/s : value/s}});
*/
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set:{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact:{
				phone: "12345678",
				email: "imrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set:
		{
			firstName: "Jenny"
		}
	}
)

db.users.updateMany(
	{department: "none"},
	{
		$set:
		{
			department: "HR"
		}
	}
)





// -----------------------------------------------------------------
// [SECTION] Delete (DELETE)
/*
	Syntax:
		- db.collectionName.deleteOne({field: value});
		- db.collectionName.deleteMany({field: valie});
*/

db.users.deleteOne({
	firstName: "Jane"
});

db.users.deleteOne({
	firstName: "Stephen"
});

db.users.deleteOne({
	firstName: "Neil"
});

db.users.deleteMany({
	department: "HR"
})

db.users.find();



// ----------------------------------------------
// [SECTION] Replace
// replaces all the fields in a document/object
/*
	Syntax:
		- db.collectionName.replaceOne(({criteria},{field: value}));
*/

db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Elon",
		lastName: "Musk",
		age: 30,
		courses: ["PHP", "Laravel", "HTML"],
		status: "trippings"
	}
)